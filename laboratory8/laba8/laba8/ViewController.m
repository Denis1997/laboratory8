//
//  ViewController.m
//  laba8
//
//  Created by fpmi on 16.05.16.
//  Copyright (c) 2016 fpmi. All rights reserved.
//

#import "ViewController.h"
#import "MapKit/MapKit.h"

@interface ViewController ()
{
    int isCity;
    MKPointAnnotation *annotationFrom;
    MKPointAnnotation *annotationTo;
}

@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UITextField *cityFrom;
@property (weak, nonatomic) IBOutlet UITextField *cityTo;

@end

@implementation ViewController
- (IBAction)editingFrom:(id)sender {
    isCity = 0;
}
- (IBAction)editingTo:(id)sender {
    isCity = 1;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UILongPressGestureRecognizer *longPressGesture =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [self.map addGestureRecognizer:longPressGesture];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) handleLongPressGesture: (UIGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [sender locationInView:self.map];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        CLLocationCoordinate2D coord = [self.map convertPoint:point toCoordinateFromView:self.map];
        CLLocation *location = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
        [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
            if (error)
            {
                NSLog(@"Geocode failed with error: %@", error);
                return ;
            }
            for (CLPlacemark * placemark in placemarks)
            {
                [self setAnnotationToMap:isCity :placemark.locality:coord];
            }
        }];
    }
}

-(void)setAnnotationToMap: (int)type :(NSString *) title :(CLLocationCoordinate2D)coordinate
{
    if (type == 0)
    {
        [_map removeAnnotation:annotationFrom];
        annotationFrom = [[MKPointAnnotation alloc] init];
        annotationFrom.title = title;
        annotationFrom.coordinate = coordinate;
        [_map addAnnotation:annotationFrom];
        self.cityFrom.text = title;
    }
    else
    {
        [_map removeAnnotation:annotationTo];
        annotationTo = [[MKPointAnnotation alloc] init];
        annotationTo.title = title;
        annotationTo.coordinate = coordinate;
        [_map addAnnotation:annotationTo];
        self.cityTo.text = title;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showFlights:(id)sender {
//    FlithsViewController *flights = [[FlithsViewController alloc] initWithBothCity: self.textFrom.text :self.textTo.text];
//    [self presentViewController: flights animated:YES completion:nil];
}

@end

